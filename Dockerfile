FROM php:7.0-apache
RUN apt-get update && \
    apt-get install -y \
    build-essential \
	wget \
	curl \
	git \
	&& rm -rf /var/lib/apt/lists/*

COPY src/ /var/www/html
EXPOSE 80